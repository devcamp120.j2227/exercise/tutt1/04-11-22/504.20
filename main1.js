import Person from "./Person1.js"
import Employee from "./Employee.js"

let person1 = new Person("Hà Quang Tiến", 24, "Nam");
person1.getPersonInfo();
console.log(person1 instanceof Person);
///////////////////////////////////
let employee1 = new Employee("Hà Quang Tiến", 24, "Nam", "Tencent", "2000$", "Lập trình viên");
employee1.getPersonInfo();
employee1.getEmployeInfo();
employee1.getSalary();
employee1.getPosition();
console.log(employee1 instanceof Person);