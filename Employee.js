import Person from "./Person1.js"

class Employee extends Person {
    constructor(personName,personAge,gender,employer,salary,position) {
        super(personName,personAge,gender)
        this.employer = employer
        this.salary = salary
        this.position = position
    }
    getEmployeInfo() {
        console.log("Chức vụ: " + this.position);
        console.log("Lương: " + this.salary);
        console.log("Nhà tuyển dụng: " + this.employer);
    }
    getSalary() {
        return this.salary
    }
    setSalary(paramSalary) {
        this.salary = paramSalary
    }
    setPosition(paramPosition) {
        this.position = paramPosition
    }
    getPosition() {
        return this.position
    }
    
}

export default Employee